#FROM docker.io/node:14.18-alpine
FROM default-route-openshift-image-registry.apps.openshiftuat.baf.com/openshift/nodejs:latest
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
RUN npm install react-scripts@3.4.1 -g --silent
COPY . ./
RUN npm run build

# production environment
#FROM nginx:stable-alpine
FROM default-route-openshift-image-registry.apps.openshiftuat.baf.com/openshift/nginx:latest
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
